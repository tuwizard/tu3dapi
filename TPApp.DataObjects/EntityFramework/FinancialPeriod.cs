//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TPApp.DataObjects.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class FinancialPeriod
    {
        public FinancialPeriod()
        {
            this.LiveDataFacts = new HashSet<LiveDataFact>();
            this.StaggingFacts = new HashSet<StaggingFact>();
        }
    
        public int Id { get; set; }
        public Nullable<System.DateTime> BeginningDate { get; set; }
        public System.DateTime EndingDate { get; set; }
        public int Period { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
        public int WeekOfPeriod { get; set; }
        public string FormattedDate { get; set; }
    
        public virtual ICollection<LiveDataFact> LiveDataFacts { get; set; }
        public virtual ICollection<StaggingFact> StaggingFacts { get; set; }
    }
}
