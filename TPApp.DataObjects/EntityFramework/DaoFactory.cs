﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.DataObjects.EntityFramework
{
    // Data access Obj factory: Factory pattern 
    public class DaoFactory : IDaoFactory
    {
        public IFactTypeSectionDao FactTypeSectionDao { get { return new FactTypeSectionDao(); } }

        public IFactTypeDao FactTypeDao { get { return new FactTypeDao(); } }

    }
}
