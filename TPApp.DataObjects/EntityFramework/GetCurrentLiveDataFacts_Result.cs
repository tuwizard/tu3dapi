//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TPApp.DataObjects.EntityFramework
{
    using System;
    
    public partial class GetCurrentLiveDataFacts_Result
    {
        public int LocationId { get; set; }
        public int FinancialPeriodId { get; set; }
        public int FactTypeId { get; set; }
        public int FactTypeSectionId { get; set; }
        public int FactTypeSubGroupId { get; set; }
        public string FactTypeName { get; set; }
        public string Description { get; set; }
        public Nullable<double> Value { get; set; }
        public string FormattedValue { get; set; }
        public Nullable<int> Score { get; set; }
        public System.DateTime EndingDate { get; set; }
        public double ScorableValue { get; set; }
        public Nullable<decimal> PointsEarned { get; set; }
        public Nullable<decimal> PointsAllowed { get; set; }
        public int FactTypeOrder { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
        public int Period { get; set; }
        public double Value2 { get; set; }
        public double Value3 { get; set; }
        public double Value4 { get; set; }
        public string FormattedDate { get; set; }
        public string FormattedScore { get; set; }
        public string PointsEarnedAverage { get; set; }
        public Nullable<double> ChartLineLowerBound { get; set; }
        public Nullable<double> ChartLineUpperBound { get; set; }
        public string FormatChartLineLowerBoundColor { get; set; }
        public string FormatChartLineUpperBoundColor { get; set; }
        public Nullable<double> PeriodValue { get; set; }
        public Nullable<double> PeriodScorableValue { get; set; }
        public Nullable<int> PeriodScore { get; set; }
        public Nullable<decimal> PeriodPointsEarned { get; set; }
        public Nullable<decimal> PeriodPointsAllowed { get; set; }
        public string Caption { get; set; }
        public int FormatDecimalsDisplayed { get; set; }
        public string FormatAppendedBy { get; set; }
        public int ParentId { get; set; }
        public string Cuid { get; set; }
    }
}
