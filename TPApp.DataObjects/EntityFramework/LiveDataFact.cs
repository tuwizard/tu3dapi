//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TPApp.DataObjects.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class LiveDataFact
    {
        public int Id { get; set; }
        public int FactTypeId { get; set; }
        public int FinancialPeriodId { get; set; }
        public int LocationId { get; set; }
        public double Value { get; set; }
        public double ScorableValue { get; set; }
        public Nullable<int> Score { get; set; }
        public Nullable<decimal> PointsEarned { get; set; }
        public Nullable<decimal> PointsAllowed { get; set; }
        public double Value2 { get; set; }
        public double Value3 { get; set; }
        public double Value4 { get; set; }
    
        public virtual FinancialPeriod FinancialPeriod { get; set; }
    }
}
