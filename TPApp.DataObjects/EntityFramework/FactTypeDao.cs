﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using TPApp.BusinessObjects;

namespace TPApp.DataObjects.EntityFramework
{
    public class FactTypeDao : IFactTypeDao
    {
        static FactTypeDao() // create contructor that has mapping entity to BO class and reverse
        {
            Mapper.CreateMap<FactType, TPApp.BusinessObjects.FactType>();
            Mapper.CreateMap<TPApp.BusinessObjects.FactType, FactType>();
        }

        public List<TPApp.BusinessObjects.FactType> GetFactTypes()
        {
            using ( var actionContext = new SS_DashboardWarehouse_V2Entities())
            {
                var factTypes = actionContext.FactTypes.AsQueryable().Where("IsActive=1").OrderBy("[FactTypeSectionId],[Order]").ToList();  // get list of data of EF entity: context.entity.AsLinqQuery().SqlStuff.ToList()
                return Mapper.Map<List<FactType>, List<TPApp.BusinessObjects.FactType>>(factTypes);
            }
        }

        public TPApp.BusinessObjects.FactType GetFactType(int factTypeId)
        {
            using ( var actionContext = new SS_DashboardWarehouse_V2Entities())
            {
                var factType = actionContext.FactTypes.FirstOrDefault(ft => ft.Id == factTypeId) as FactType;   // get one rec from EF entity: context.entity.FirstOrDefault(sql linq stuff) as this entity
                return Mapper.Map<FactType, TPApp.BusinessObjects.FactType>(factType);  // mapping to BO class from EF entity
            }
        }

    }
}
