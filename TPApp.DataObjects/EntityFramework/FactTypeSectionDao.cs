﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using TPApp.BusinessObjects;

namespace TPApp.DataObjects.EntityFramework
{
    public class FactTypeSectionDao : IFactTypeSectionDao
    {
        static FactTypeSectionDao()
        {
            Mapper.CreateMap<FactTypeSection, TPApp.BusinessObjects.FactTypeSection>();  // Mapping entity in EF to BO class and reverse
            Mapper.CreateMap<TPApp.BusinessObjects.FactTypeSection, FactTypeSection>();
        }

        // Methods implementation for I
        public List<TPApp.BusinessObjects.FactTypeSection> GetFactTypeSections()
        {
            List<TPApp.BusinessObjects.FactTypeSection> retList = new List<TPApp.BusinessObjects.FactTypeSection>();
            using (var actionContext = new SS_DashboardWarehouse_V2Entities())
            {
                var factTypeSections = actionContext.FactTypeSections.AsQueryable().OrderBy("[Order]").ToList();  // Get list data from entity: context.entity.AsLinqQuery().SqlStuff.ToList()
                retList = Mapper.Map<List<FactTypeSection>, List<TPApp.BusinessObjects.FactTypeSection>>(factTypeSections); //exec mapping from list of obj in EF to list of BO
            }

            return retList;
        }

        public TPApp.BusinessObjects.FactTypeSection GetFactTypeSection(int factTypeSectionId)
        {
            using (var actionContext = new SS_DashboardWarehouse_V2Entities())
            {
                var factTypeSection = actionContext.FactTypeSections.FirstOrDefault(fts => fts.Id == factTypeSectionId) as FactTypeSection;  // Get one rec from EF entity: context.entity.FirstOrDefault(linq sql stuff) as entity
                return Mapper.Map<FactTypeSection, TPApp.BusinessObjects.FactTypeSection>(factTypeSection);
            }
        }

    }
}
