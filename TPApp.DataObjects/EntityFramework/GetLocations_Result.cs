//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TPApp.DataObjects.EntityFramework
{
    using System;
    
    public partial class GetLocations_Result
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string LocationCuid { get; set; }
        public int ParentOrder { get; set; }
        public Nullable<decimal> PointsEarned { get; set; }
        public Nullable<decimal> PointsAllowed { get; set; }
        public string PointsEarnedAverage { get; set; }
        public int ParentId { get; set; }
        public string LocationType { get; set; }
    }
}
