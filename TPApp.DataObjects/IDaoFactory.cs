﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.DataObjects
{
    // Abstract factory I: Create DAO
    // Factory pattern
    public interface IDaoFactory
    {
        IFactTypeSectionDao FactTypeSectionDao { get; }
        IFactTypeDao FactTypeDao { get; }
        //IFactDao FactDao { get; }
        //ILocationDao LocationDao { get; }
    }
}
