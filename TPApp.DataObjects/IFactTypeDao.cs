﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPApp.BusinessObjects;

namespace TPApp.DataObjects
{
    // Defines set of Interfaces that is used by Services layer that will then feed/provide to UI Api/Odata controller 
    // Define methods to access FactTypes
    // This is a db-independent interface. Implementations are db specific.
    public interface IFactTypeDao
    {
        // get a list of FactTypes
        List<FactType> GetFactTypes();

        // get a FactType by FactTypeId
        FactType GetFactType(int factTypeId);
    }     
}
