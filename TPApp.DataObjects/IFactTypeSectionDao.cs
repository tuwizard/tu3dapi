﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPApp.BusinessObjects;

namespace TPApp.DataObjects
{
    // Define set of Interfaces that will be used by the Service Layer that then will feed end point to  UI Api/Odada Controller
    // Defines methods to access FactTypeSections
    // This is db-independent interface. Implementation are db specific.
    // DAO pattern
    public interface IFactTypeSectionDao
    {
        // get list of FactTypeSection/Cat
        List<FactTypeSection> GetFactTypeSections();

        // get a FactTypeSection by Id
        FactTypeSection GetFactTypeSection(int factTypeSectionId);
    }
}
