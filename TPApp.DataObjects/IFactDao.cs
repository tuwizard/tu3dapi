﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPApp.BusinessObjects;

namespace TPApp.DataObjects
{
    // DAO design pattern
    // Define a set of I that will be used by Service layer
    // Definse methods to access fact data
    // this is DB-independent I, the implementation are db specific
    public interface IFactDao
    {
        // get list of fact
        List<Fact> GetAllFact();

        // get list of fact by fact type section id and location id
        List<Fact> GetFactBySectionIdAndLocationId(int locationId, int SectionId);

        // get fact by financial period id
        List<Fact> GetFactByFinancialPeriodId(int financialPeriodId);
    }
}
