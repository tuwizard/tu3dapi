﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPApp.BusinessObjects;

namespace TPApp.DataObjects
{
    // DAO pattern
    // Define a set of I that will be used by service layer
    // Define methods to access location data
    // this is db-independent I, implementation is db specific
    public interface ILocationDao
    {
        // get list of location 
        List<Location> GetLocations(int locationId);

        // get a location
        Location GetLocation(int locationId);
    }
}
