﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using TPApp.Models.Models;

namespace TPApp.DataObjects
{
    public class TPAppDbContext : DbContext
    {
        public TPAppDbContext()
          : base("name=TPAppDbContext")
        {

        }

        public static TPAppDbContext Create()
        {
            return new TPAppDbContext();
        }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
    }
}
