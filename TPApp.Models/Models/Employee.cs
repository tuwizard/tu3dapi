﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.Models.Models
{
    public class Employee
    {
        public int? EmployeeId { get; set; }
        public string LoginUserName { get; set; }
        public string Password { get; set; }
        public bool UseStripesNetSecurity { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool Active { get; set; }
        public bool CanClockIn { get; set; }
        public int? CurrentHomeStoreLocationId { get; set; }
        public int? CurrentJobCodeId { get; set; }
    }
}
