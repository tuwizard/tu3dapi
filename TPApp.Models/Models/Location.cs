﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.Models.Models
{
    public class Location
    {
        public int? location_id { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        public string location_cuid { get; set; }
        public string location_name { get; set; }
        public string location_timezone { get; set; }
        public string location_type { get; set; }
        public int? location_parent_id { get; set; }
        public int? location_is_active { get; set; }
        public string location_peoplesoft_deptid { get; set; }
        public string location_address { get; set; }
        public string location_city { get; set; }
        public string location_state { get; set; }
        public string location_zip { get; set; }
        public int? Location_Manager_id { get; set; }
        public int? location_timezone_id { get; set; }

    }
}
