﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPApp.Models.Models;

namespace TPApp.Services.Common
{
    public interface IUserService 
    {
        IEnumerable<Employee> GetUser();
    }
}
