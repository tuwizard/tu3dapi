﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPApp.DataObjects;
using TPApp.Models.Models;

namespace TPApp.Services.Common
{
    public class UserService : IUserService
    {
        private readonly TPAppDbContext _dbContext;

        public UserService(TPAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
             
        public IEnumerable<Employee> GetUser()
        {
            return _dbContext.Set<Employee>().ToList();
        }

    }
}
