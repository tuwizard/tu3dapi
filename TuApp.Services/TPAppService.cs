﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using TPApp.BusinessObjects;
using TPApp.DataObjects;

namespace TPApp.Services
{
    // Implementation of ITPAppService interface: It can handle different data provider, factory

    // ** Facade pattern.
    // ** Repository pattern (Service could be split up in individual Repositories: FactType, FactTypeSection, etc).
    public class TPAppService : ITPAppService
    {
        static readonly string provider = ConfigurationSettings.AppSettings.Get("DataProvider");  //-- Get which db factory the app're going to use (AdoNet, linqSql, or EF), default is EF
        static readonly IDaoFactory factory = DaoFactories.GetFactory(provider);
         
        // Available Repositories from DA and BO 
        static readonly IFactTypeSectionDao factTypeSectionDao = factory.FactTypeSectionDao;
        static readonly IFactTypeDao factTypeDao = factory.FactTypeDao;

        // Services will be servered to UI Api/Odata controllers
        // 1- FactTypeSection Services
        public List<FactTypeSection> GetFactTypeSections()
        {
            return factTypeSectionDao.GetFactTypeSections();
        }
        
        public FactTypeSection GetFactTypeSection(int factTypeSectionId)
        {
            return factTypeSectionDao.GetFactTypeSection(factTypeSectionId);
        }

        // 2- FactType services
        public List<FactType> GetFactTypes()
        {
            return factTypeDao.GetFactTypes();
        }

        public FactType GetFactType(int factTypeId)
        {
            return factTypeDao.GetFactType(factTypeId);
        }

        // 3- Location services


        // 4- Fact services
    }
}
