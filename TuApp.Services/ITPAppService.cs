﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPApp.BusinessObjects;

namespace TPApp.Services
{
    // This is single I that host all repositories
    public interface ITPAppService
    {
        // FactTypeSection repository
        List<FactTypeSection> GetFactTypeSections();
        FactTypeSection GetFactTypeSection(int factTypeSectionId);

        // FactType repository
        List<FactType> GetFactTypes();
        FactType GetFactType(int factTypeId);

        // Fact repository


        // Location repository


        /* Employee Repository
        Employee GetEmployee(int employeeId);
        Employee GetEmployeeByEmail(string email);
        List<Employee> GetEmployee(string sortExpression);
        Employee GetEmployeeByOrder(int orderId);
        List<Employee> GetEmployeesWithOrderStatistics(string sortExpression);
        void InsertEmployee(Employee employee);
        void UpdateEmployee(Employee employee);
        void DeleteEmployee(Employee employee); */
    }
}
