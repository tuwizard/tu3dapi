﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.OData;
using TPApp.DataObjects;
using TPApp.Models.Models;
using TPApp.Services.Common;

namespace TPApp.Api.Controllers.Common
{
    public class UsersController : ODataController
    {
        private TPAppDbContext _dbContext;
        private UserService _userService;

        public UsersController(TPAppDbContext dbContext, UserService userService) 
            //: base(dbContext, userService) 
        {
            _dbContext = dbContext;
            _userService = userService;
        }

        [EnableQuery]
        public IQueryable<Employee> Get()
        {
            return _dbContext.Set<Employee>();
        }

        public IEnumerable<Employee> GetUser()
        {
            return _userService.GetUser();
        }
    }

    /* 
    [EnableQuery]
    public IQueryable<Product> Get()
    {
        return db.Products;
    }
    [EnableQuery]
    public SingleResult<Product> Get([FromODataUri] int key)
    {
        IQueryable<Product> result = db.Products.Where(p => p.Id == key);
        return SingleResult.Create(result);
    }
     */

    /*
    public class ProductsController : ODataController
    {
        ProductsContext db = new ProductsContext();
        private bool ProductExists(int key)
        {
            return db.Products.Any(p => p.Id == key);
        } 
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
    */
     
}