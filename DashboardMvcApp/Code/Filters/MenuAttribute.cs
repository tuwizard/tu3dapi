﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DashboardMvcApp.Code
{
    // sets selected menu item
    // Inherite from ActionFilterAttribute then you can override OnActionExecuting to inject filter context you need b4 pushing to the View (pre-post)
    public class MenuAttribute : ActionFilterAttribute
    {
        MenuItem selectedMenu;

        public MenuAttribute(MenuItem selectedMenu)
        {
            this.selectedMenu = selectedMenu;
        }

        // sets selected menu in ViewData
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewData["SelectedMenu"] = selectedMenu.ToString().ToLower();
        }
    }
}