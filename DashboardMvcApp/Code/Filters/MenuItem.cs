﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DashboardMvcApp.Code
{
    public enum MenuItem
    {
        Home,
        DashboardHQ,
        KPICategoryAdmin,
        KPIAdmin,
        Location,
        FactTypeSections,
        FactTypes,
        Facts
    }
}