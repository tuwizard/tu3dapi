﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace DashboardMvcApp.Code
{
    //Sortable I: define col and order
    public interface ISortable : IEnumerable
    {
        string Sort { get; }
        string Order { get; }
    }

    // Generic form of ISortable I. 
    public interface ISortable<T>: ISortable, IEnumerable<T>
    {
        
    }
}