﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DashboardMvcApp.Code
{
    // Sortable list: support sorting of lists on page
    public class SortedList<T>: ISortable<T>
    {
        public List<T> List { get; private set; }
        public string Sort { get; private set; }
        public string Order { get; private set; }

        // 
        public SortedList(List<T> list, string sort = null, string order = null)
        {
            List = list;
            Sort = sort;
            Order = order;
        }

        #region IEnumerable Members  
        public System.Collections.IEnumerator GetEnumerator()   //Allow loop through non-generic collection
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IEnumberable<T> Members
        IEnumerator<T> IEnumerable<T>.GetEnumerator()  //I out T, Allow iteration over col of type T
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    
}