﻿using TPApp.BusinessObjects;
using TPApp.Services;
using DashboardMvcApp.Areas.Dashboard.Models;
using DashboardMvcApp.Code;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DashboardMvcApp.Areas.Dashboard.Controllers
{
    public class DashboardController : Controller
    {
        ITPAppService tpAppService { get; set; }

       // static constructor. establishes Automapper object maps
        static DashboardController()
        {
            Mapper.CreateMap<FactTypeSection, FactTypeSectionModel>();
                //.ForMember(dest => dest.UnitPrice, opt => opt.MapFrom(src => string.Format("{0:C}", src.UnitPrice)));
            Mapper.CreateMap<FactTypeSectionModel, FactTypeSection>();
        }

        // Default constructor
        public DashboardController() : this(new TPAppService()) 
        { 

        }

        // Overloaded 'injectable' constructor
        // ** Constructor Dependency Injection (DI).
        public DashboardController(ITPAppService tpAppService) 
        {
            this.tpAppService = tpAppService;
        }

        // Action for View DashboardHQ area page
        [HttpGet]
        public ActionResult DashboardHQ()
        {
            //Use ... Get dynamic view dictionary data as methods to expose data to View from Model/result return from controller
            ViewBag.Crumbs = new List<BreadCrumb>();
            ViewBag.Crumbs.Add(new BreadCrumb { Title = "Home", Url = "/" });
            ViewBag.Crumbs.Add(new BreadCrumb { Title = "Dashboard HQ" });

            ViewBag.Menu = "DashboardHQ";

            return View();
        }

        // Action for View KPICategoryAdmin page
        [HttpGet]
        public ActionResult KPICategoryAdmin()
        {
            ViewBag.Crumbs = new List<BreadCrumb>();
            ViewBag.Crumbs.Add(new BreadCrumb { Title = "Home", Url = "/" });
            ViewBag.Crumbs.Add(new BreadCrumb { Title = "KPI Category" });

            ViewBag.Menu = "KPICategoryAdmin";

            return View();
        }

        // Action for View KPI Admin page
        [HttpGet]
        public ActionResult KPIAdmin()
        {
            ViewBag.Crumbs = new List<BreadCrumb>();
            ViewBag.Crumbs.Add(new BreadCrumb { Title = "Home", Url = "/" });
            ViewBag.Crumbs.Add(new BreadCrumb { Title = "KPIs" });

            ViewBag.Menu = "KPIAdmin";

            return View();
        }

        /*
        //
        // GET: /Dashboard/Dashboard/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Dashboard/Dashboard/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Dashboard/Dashboard/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Dashboard/Dashboard/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Dashboard/Dashboard/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Dashboard/Dashboard/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Dashboard/Dashboard/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Dashboard/Dashboard/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
         */
    }
}
