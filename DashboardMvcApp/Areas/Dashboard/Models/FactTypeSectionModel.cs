﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DashboardMvcApp.Areas.Dashboard.Models
{
    // FactTypeSection ViewModel (diff. BO)
    // Data Tranfer Object Pattern: DTO

    public class FactTypeSectionModel
    {
        // Identity Field Pattern
        public int Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public bool ShowScore { get; set; }
        public bool IsActive { get; set; }


    }
}