﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using DashboardMvcApp.Code;

namespace DashboardMvcApp.Areas.Dashboard.Models
{
    // FactTypeSections ViewModel
    // Data Transfer Object Pattern: DTO

    public class FactTypeSectionsModel
    {
        // Sortable list of sections
        public SortedList<FactTypeSectionModel> FactTypeSections { get; set; }
    }
}