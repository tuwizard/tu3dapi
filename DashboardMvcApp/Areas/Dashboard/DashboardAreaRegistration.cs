﻿using System.Web.Mvc;

namespace DashboardMvcApp.Areas.Dashboard
{
    public class DashboardAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Dashboard";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            //--In its area, define each action belong to its area and controller: eg. Area: Dashboard ==> Controller: Dashboard : Define a set of its actions that we want to do on this area.
            /*   
            HQ page, this is where users start viewing KPI reports. They can browse and/or search a category/sections of KPI.  
            The app demonstrates navigation patterns: eg. searching, filtering, sorting, and master-detail. The master-detail paradigm is demonstrated by FT Sections, FT, or F pages. 
            The list of those FT sections/FT/F represents the master and the FTS/FT/F details page represents the detail.
             */
            //----1--Action for HQ page
            context.MapRoute(
                "",
                "DashboardHQ",
                new { area = "Dashboard", controller = "Dashboard", action = "DashboardHQ"}
            );

            //----2--Action for KPICategoryAdmin/FT Sections page: Providing/Offering listing, searching, filtering, sorting on FT Sections and be able to view/add/edit FT Section detail.
            context.MapRoute(
                "",
                "KPICategoryAdmin",
                new { area = "Dashboard", controller = "Dashboard", action = "KPICategoryAdmin"}
            );

            //----3--Action for KPI name/FT page: Provide/Offering listing, searching, filtering, sorting on KPI/FT and be able to view/add/edit KPI detail/property.
            context.MapRoute(
                "",
                "KPIAdmin",
                new { area = "Dashboard", controller = "Dashboard", action = "KPIAdmin"}
            );
        }

        /* 
          context.MapRoute("", "shopping", new { area = "Shop", controller = "Shop", action = "Shopping" });
            context.MapRoute("", "products", new { area = "Shop", controller = "Shop", action = "Products" });
            context.MapRoute("", "products/{id}", new { area = "Shop", controller = "Shop", action = "Product" });
            context.MapRoute("", "search", new { area = "Shop", controller = "Shop", action = "Search" });         
         
         */
        /*context.MapRoute(
            "Dashboard_default",
            "Dashboard/{controller}/{action}/{id}",
            new { action = "Index", id = UrlParameter.Optional }
        );*/

    }
}
