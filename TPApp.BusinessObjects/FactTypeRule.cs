﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.BusinessObjects
{
    public class FactTypeRule: BusinessObject
    {
        public int Id { get; set; }
        public int FactTypeId { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public int Score { get; set; }
        public int ProcessingOrder { get; set; }
        public string ValueToUse { get; set; }

        // EDP: Foreign key mapping: FactType is the parent
        public virtual FactType FactType { get; set; }
    }
}
