﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.BusinessObjects
{
    // Fact class
    // Enterprise Design Pattern: Domain Model
    public class Fact: BusinessObject
    {
        // Enterprise Design Pattern: Identity field pattern
        public int LocationId { get; set; }
        public int FinancialPeriodId { get; set; }
        public int FactTypeId { get; set; }
        public int FactTypeSectionId { get; set; }
        public int FactTypeSubGroupId { get; set; }
        public string FactTypeName { get; set; }
        public string Description { get; set; }
        public Nullable<double> Value { get; set; }
        public string FormattedValue { get; set; }
        public Nullable<int> Score { get; set; }
        public int FactTypeOrder { get; set; }
        public double Value2 { get; set; }
        public double Value3 { get; set; }
        public double Value4 { get; set; }
        public string FormattedDate { get; set; }
        public string PointsEarnedAverage { get; set; }
        public Nullable<double> ChartLineLowerBound { get; set; }
        public Nullable<double> ChartLineUpperBound { get; set; }
        public Nullable<double> PeriodValue { get; set; }
        public Nullable<int> PeriodScore { get; set; }
        public string Caption { get; set; }
        public int FormatDecimalsDisplayed { get; set; }
        public string FormatAppendedBy { get; set; }
        public int ParentId { get; set; }
        public string Cuid { get; set; }

        public virtual FactTypeSection FactTypeSection { get; set; }
        public virtual FactTypeSubGroup FactTypeSubGroup { get; set; }
        public virtual FactType FactType { get; set; }
        public virtual Location Location { get; set; }

        //public int LocationId { get; set; }
        //public int FinancialPeriodId { get; set; }
        //public int FactTypeId { get; set; }
        //public int FactTypeSectionId { get; set; }
        //public int FactTypeSubGroupId { get; set; }
        //public string FactTypeName { get; set; }
        //public string Description { get; set; }
        //public Nullable<double> Value { get; set; }
        //public string FormattedValue { get; set; }
        //public Nullable<int> Score { get; set; }
        ////public System.DateTime EndingDate { get; set; }
        ////public double ScorableValue { get; set; }
        ////public Nullable<decimal> PointsEarned { get; set; }
        ////public Nullable<decimal> PointsAllowed { get; set; }
        //public int FactTypeOrder { get; set; }
        ////public int Week { get; set; }
        ////public int Year { get; set; }
        ////public int Period { get; set; }
        //public double Value2 { get; set; }
        //public double Value3 { get; set; }
        //public double Value4 { get; set; }
        //public string FormattedDate { get; set; }
        ////public string FormattedScore { get; set; }
        //public string PointsEarnedAverage { get; set; }
        //public Nullable<double> ChartLineLowerBound { get; set; }
        //public Nullable<double> ChartLineUpperBound { get; set; }
        ////public string FormatChartLineLowerBoundColor { get; set; }
        ////public string FormatChartLineUpperBoundColor { get; set; }
        //public Nullable<double> PeriodValue { get; set; }
        ////public Nullable<double> PeriodScorableValue { get; set; }
        //public Nullable<int> PeriodScore { get; set; }
        ////public Nullable<decimal> PeriodPointsEarned { get; set; }
        ////public Nullable<decimal> PeriodPointsAllowed { get; set; }
        //public string Caption { get; set; }
        //public int FormatDecimalsDisplayed { get; set; }
        //public string FormatAppendedBy { get; set; }
        //public int ParentId { get; set; }
        //public string Cuid { get; set; }

        //"D": "9/8/2013", "V": 72, "CUBnd": 90, "CLBnd": 75, "LocId": 1027, "FT": 19
        //public string D { get; set; }
       // public double V { get; set; }
        //public double? CUBnd { get; set; }
        //public double? CLBnd { get; set; }
        //public int LocId { get; set; }
        //public int FT { get; set; }
        //public double V2 { get; set; }
        //public double V3 { get; set; }
        //public double V4 { get; set; }
       // public int ParentId { get; set; }
        //public string Cuid { get; set; }

    }
}
