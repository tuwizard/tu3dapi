﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.BusinessObjects
{
    // FactTypeSection class
    // Enterprise Design Pattern: Domain Model
    public class FactTypeSection : BusinessObject
    {
        // Identity Field pattern
        public int Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public bool ShowScore { get; set; }
        public bool IsActive { get; set; }

        // Enterprise design pattern: 
        public virtual ICollection<FactType> FactTypes { get; set; }

    }
}
