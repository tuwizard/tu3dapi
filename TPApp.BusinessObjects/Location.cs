﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.BusinessObjects
{
    // Location class
    // Enterprise Design pattern: Domain Model
    public class Location: BusinessObject
    {
        // Enterprise Design pattern: Identity field pattern
        public int Id { get; set; }
        public string Cuid { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public Nullable<int> ParentId { get; set; }
        public bool IsActive { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
