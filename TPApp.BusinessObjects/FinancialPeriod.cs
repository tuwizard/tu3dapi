﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPApp.BusinessObjects
{
    public class FinancialPeriod: BusinessObject
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> BeginningDate { get; set; }
        public System.DateTime EndingDate { get; set; }
        public int Period { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
        public int WeekOfPeriod { get; set; }
        public string FormattedDate { get; set; }

        public virtual ICollection<Fact> Facts { get; set; }
    }
}
